package com.example.nileshkumarpatelnycschools.viewmodels

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.nileshkumarpatelnycschools.data.models.School
import com.example.nileshkumarpatelnycschools.data.repositories.SchoolsRepository
import com.example.nileshkumarpatelnycschools.data.*
import kotlinx.coroutines.launch
import javax.inject.Inject

class SchoolViewModel @Inject constructor(private val repository: SchoolsRepository) : ViewModel() {

    val schoolList = MutableLiveData<Result<List<School>>>()

    fun fetchSchool() : MutableLiveData<Result<List<School>>> {
        viewModelScope.launch {
            schoolList.postValue(Result.LOADING)
            try {
                val mResult = repository.getSchools()
                schoolList.postValue(Result.SUCCESS(mResult))
            } catch (throwable: Throwable) {
                schoolList.postValue(Result.ERROR(throwable.message))
            }

        }
        return schoolList
    }
}
