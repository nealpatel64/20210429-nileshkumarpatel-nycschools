package com.example.nileshkumarpatelnycschools.viewmodels

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.nileshkumarpatelnycschools.data.*
import com.example.nileshkumarpatelnycschools.data.models.Score
import com.example.nileshkumarpatelnycschools.data.repositories.ScoreRepository
import kotlinx.coroutines.launch
import javax.inject.Inject

class ScoreViewModel @Inject constructor(private val repository: ScoreRepository) : ViewModel() {

    private val score = MutableLiveData<Result<List<Score>>>()

    fun fetchScore(dbn : String) : MutableLiveData<Result<List<Score>>> {
        viewModelScope.launch {
            score.postValue(Result.LOADING)
            try {
                val mResult = repository.getScoresData(dbn)
                score.postValue(Result.SUCCESS(mResult))
            } catch (e: Throwable) {
                score.postValue(Result.ERROR(e.message))
            }
        }
        return score
    }

}
