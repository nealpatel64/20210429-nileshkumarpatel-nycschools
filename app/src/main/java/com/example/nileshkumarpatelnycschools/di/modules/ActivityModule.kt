package com.example.nileshkumarpatelnycschools.di.modules

import com.example.nileshkumarpatelnycschools.di.subComponents.MainActivitySubComponent
import com.example.nileshkumarpatelnycschools.ui.activity.MainActivity
import dagger.Binds
import dagger.Module
import dagger.android.AndroidInjector
import dagger.multibindings.ClassKey
import dagger.multibindings.IntoMap

/**
 * Map all our activities here.
 * Compile time dagger will know about our activities
 */
@Module(subcomponents = [MainActivitySubComponent::class])
abstract class ActivityModule {

    @Binds
    @IntoMap
    @ClassKey(MainActivity::class)
    abstract fun bindActivity(factory : MainActivitySubComponent.Factory) : AndroidInjector.Factory<*>

}