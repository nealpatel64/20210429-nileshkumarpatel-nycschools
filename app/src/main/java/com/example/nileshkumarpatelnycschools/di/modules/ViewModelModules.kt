package com.example.nileshkumarpatelnycschools.di.modules

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.nileshkumarpatelnycschools.di.scopes.ViewModelKey
import com.example.nileshkumarpatelnycschools.viewmodels.SchoolViewModel
import com.example.nileshkumarpatelnycschools.viewmodels.ScoreViewModel
import com.example.nileshkumarpatelnycschools.viewmodels.ViewModelFactoryProvider
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

/**
 * Map all our viewModels here.
 * Dagger know our ViewModels in compile time
 */

@Module
abstract class ViewModelModules {

    @Binds
    @IntoMap
    @ViewModelKey(SchoolViewModel::class)
    abstract fun bindSchoolViewModel(fragmentViewModel: SchoolViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(ScoreViewModel::class)
    abstract fun bindScoreViewModel(fragmentViewModel: ScoreViewModel): ViewModel

    @Binds
    abstract fun bindViewModelFactory (viewModelFactory: ViewModelFactoryProvider) : ViewModelProvider.Factory

}