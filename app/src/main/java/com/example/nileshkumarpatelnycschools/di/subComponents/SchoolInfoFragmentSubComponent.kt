package com.example.nileshkumarpatelnycschools.di.subComponents

import com.example.nileshkumarpatelnycschools.ui.fragments.SchoolInfoFragment
import dagger.Subcomponent
import dagger.android.AndroidInjector

@Subcomponent
interface SchoolInfoFragmentSubComponent : AndroidInjector<SchoolInfoFragment> {

    @Subcomponent.Factory
    interface Factory : AndroidInjector.Factory<SchoolInfoFragment>
}