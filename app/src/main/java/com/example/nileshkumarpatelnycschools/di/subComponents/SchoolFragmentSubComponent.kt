package com.example.nileshkumarpatelnycschools.di.subComponents

import com.example.nileshkumarpatelnycschools.ui.fragments.SchoolListFragment
import dagger.Subcomponent
import dagger.android.AndroidInjector

@Subcomponent
interface SchoolFragmentSubComponent : AndroidInjector<SchoolListFragment> {

    @Subcomponent.Factory
    interface Factory : AndroidInjector.Factory<SchoolListFragment>
}