package com.example.nileshkumarpatelnycschools.di.subComponents

import com.example.nileshkumarpatelnycschools.ui.activity.MainActivity
import dagger.Subcomponent
import dagger.android.AndroidInjector

@Subcomponent
interface MainActivitySubComponent : AndroidInjector<MainActivity> {

   @Subcomponent.Factory
   interface Factory : AndroidInjector.Factory<MainActivity>

}