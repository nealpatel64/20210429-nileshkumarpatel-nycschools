package com.example.nileshkumarpatelnycschools.di.modules

import com.example.nileshkumarpatelnycschools.data.api.DataService
import com.example.nileshkumarpatelnycschools.data.repositories.SchoolsRepository
import com.example.nileshkumarpatelnycschools.data.repositories.ScoreRepository
import com.example.nileshkumarpatelnycschools.ui.adapters.SchoolsAdapter
import dagger.Module
import dagger.Provides

@Module(includes = [FragmentsModule::class])
class AppModule {

    @Provides
    fun provideSchoolsRepository(service: DataService): SchoolsRepository {
        return SchoolsRepository(service)
    }

    @Provides
    fun provideScoreRepository(service: DataService): ScoreRepository {
        return ScoreRepository(service)
    }

    @Provides
    fun provideSchoolListAdapter(): SchoolsAdapter {
        return SchoolsAdapter()
    }

}