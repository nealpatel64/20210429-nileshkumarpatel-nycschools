package com.example.nileshkumarpatelnycschools.di.component

import com.example.nileshkumarpatelnycschools.BaseApplication
import com.example.nileshkumarpatelnycschools.di.modules.*
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjectionModule
import dagger.android.support.AndroidSupportInjectionModule
import javax.inject.Singleton

/**
 * Component interface used to defines the connection between provider of objects, module
 * Used to generate code which uses the modules to complete requested dependencies
 * Used Singleton Annotation to make sure create one single object
 */

@Singleton
@Component(
    modules = [AndroidInjectionModule::class, AndroidSupportInjectionModule::class,
        AppModule::class, ActivityModule::class, NetworkModule::class, FragmentsModule::class, ViewModelModules::class]
)
interface ApplicationComponent {

    @Component.Builder
    interface Builder {

        @BindsInstance
        fun application(application: BaseApplication): Builder

        fun build(): ApplicationComponent

    }

    fun inject(instance: BaseApplication?)

}