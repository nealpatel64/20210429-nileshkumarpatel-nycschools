package com.example.nileshkumarpatelnycschools.di.modules

import com.example.nileshkumarpatelnycschools.di.subComponents.SchoolFragmentSubComponent
import com.example.nileshkumarpatelnycschools.di.subComponents.SchoolInfoFragmentSubComponent
import com.example.nileshkumarpatelnycschools.ui.fragments.SchoolInfoFragment
import com.example.nileshkumarpatelnycschools.ui.fragments.SchoolListFragment
import dagger.Binds
import dagger.Module
import dagger.android.AndroidInjector
import dagger.multibindings.ClassKey
import dagger.multibindings.IntoMap

/**
 * Map all our Fragments here.
 * Dagger know our Fragments in compile time
 */

@Module(subcomponents = [SchoolFragmentSubComponent::class, SchoolInfoFragmentSubComponent::class])
abstract class FragmentsModule {

    @Binds
    @IntoMap
    @ClassKey(SchoolListFragment::class)
    internal abstract fun bindSchoolListFragment(factory: SchoolFragmentSubComponent.Factory): AndroidInjector.Factory<*>

    @Binds
    @IntoMap
    @ClassKey(SchoolInfoFragment::class)
    internal abstract fun bindSchoolDetailedFragment(factory: SchoolInfoFragmentSubComponent.Factory): AndroidInjector.Factory<*>

}