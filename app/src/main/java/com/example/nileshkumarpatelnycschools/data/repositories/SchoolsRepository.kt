package com.example.nileshkumarpatelnycschools.data.repositories

import com.example.nileshkumarpatelnycschools.data.api.DataService
import com.example.nileshkumarpatelnycschools.data.models.School

/**
 * Repository for school list remote operation,
 * We can use Paging 3 as well to handle reload, refresh and error for better performance
 * Currently NYC School supporting Paging but JSON not sharing current page number
 * Any other page details to handle in Android side, Current paging can useful with show pages like website(1-10,11-20,21-30,etc)
 * If we have API which shared required info for Android paging than we can use that to avoid waiting time
 */

class SchoolsRepository (private val service : DataService) {

    suspend fun getSchools() : List<School>{
        return service.getSchools()
    }

}