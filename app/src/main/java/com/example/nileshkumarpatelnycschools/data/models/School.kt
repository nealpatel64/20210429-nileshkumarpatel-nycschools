package com.example.nileshkumarpatelnycschools.data.models

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName

data class School(
    @SerializedName("dbn")
    val dbn: String,
    @SerializedName("school_name")
    val schoolName: String,
    @SerializedName("phone_number")
    val phoneNumber: String,
    @SerializedName("school_email")
    val schoolEmail: String,
    @SerializedName("website")
    val website: String,
    @SerializedName("overview_paragraph")
    val overViewParagraph: String
) : Parcelable {

    constructor(parcel: Parcel) : this(
        parcel.readString()!!,
        parcel.readString()!!,
        parcel.readString()!!,
        parcel.readString()!!,
        parcel.readString()!!,
        parcel.readString()!!
    )

    override fun writeToParcel(p0: Parcel?, p1: Int) {
        p0?.writeString(dbn)
        p0?.writeString(schoolName)
        p0?.writeString(phoneNumber)
        p0?.writeString(schoolEmail)
        p0?.writeString(website)
        p0?.writeString(overViewParagraph)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<School> {
        override fun createFromParcel(parcel: Parcel): School {
            return School(parcel)
        }

        override fun newArray(size: Int): Array<School?> {
            return arrayOfNulls(size)
        }
    }
}