package com.example.nileshkumarpatelnycschools.data.repositories

import com.example.nileshkumarpatelnycschools.data.api.DataService
import com.example.nileshkumarpatelnycschools.data.models.Score

/**
 * Repository for score remote operation
 */

class ScoreRepository (private val service : DataService) {

    suspend fun getScoresData(dbn : String) : List<Score>{
        return service.getScores(dbn)
    }

}