package com.example.nileshkumarpatelnycschools.data

/**
 * Used Sealed class to handle the Network response from the server (REST API Calls)
 * Behave Same like Enum with advanced version, we can use sealed class with when expression in Activity/Fragment to get response
 */

sealed class Result<out T> {

    object LOADING : Result<Nothing>()

    data class SUCCESS<T>(val data: T) : Result<T>()

    data class ERROR(val error: String?) : Result<Nothing>()

}