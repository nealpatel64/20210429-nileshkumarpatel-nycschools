package com.example.nileshkumarpatelnycschools.data.api

import com.example.nileshkumarpatelnycschools.data.models.School
import com.example.nileshkumarpatelnycschools.data.models.Score
import com.example.nileshkumarpatelnycschools.utilities.Constants
import retrofit2.http.GET
import retrofit2.http.Query

/**
 * API Service with request method and end point URL of REST API Request
 */

interface DataService {

    @GET(Constants.SCHOOL_LIST_END_URL)
    suspend fun getSchools(): List<School>

    @GET(Constants.SAT_SCORE_END_URL)
    suspend fun getScores(@Query("dbn") dbn: String): List<Score>
}