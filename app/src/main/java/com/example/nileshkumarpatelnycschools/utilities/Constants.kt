package com.example.nileshkumarpatelnycschools.utilities

/**
 * Constants utility class for common use Strings
 */
object Constants {

    const val BASE_URL = "https://data.cityofnewyork.us/"

    const val SCHOOL_LIST_END_URL = "resource/s3k6-pzi2.json"

    const val SAT_SCORE_END_URL = "resource/f9bf-2cp4.json"

    const val ROOT_FRAGMENT = "root_fragment"

    const val BUNDLE_DATA = "data"

    const val TESTING_EXPECTED_ERROR = "Network Error"

    const val TESTING_SCORE_QUERY = "01M292"

    const val WEB_VIEW_FRAGMENT = "WebViewFragment"

}