package com.example.nileshkumarpatelnycschools.ui.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Filter
import android.widget.Filterable
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.example.nileshkumarpatelnycschools.R
import com.example.nileshkumarpatelnycschools.data.models.School
import kotlinx.android.synthetic.main.adapter_school_item.view.*
import java.util.*
import javax.inject.Inject
import kotlin.collections.ArrayList

/**
 * SchoolList Adapter used with Filterable API to filter out the list
 * Used lambda onItemClicked handle click event,
 * To Improve performance, used DiffUtil API instead of clearing and adding data during filter(search)
 */

class SchoolsAdapter @Inject constructor() :
    RecyclerView.Adapter<SchoolsAdapter.MyViewHolder>(), Filterable {
    private var oldSchoolArray = ArrayList<School>()

    var data = ArrayList<School>()
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    lateinit var onItemClicked: (School) -> Unit

    inner class MyViewHolder(itemView: View) :
        RecyclerView.ViewHolder(itemView) {

        init {
            oldSchoolArray = data
            itemView.setOnClickListener {
                onItemClicked(data[adapterPosition])
            }
        }

        fun bindItem(item: School) {
            itemView.txtViewSchoolName.text = item.schoolName
            itemView.txtViewPhoneNo.text =
                itemView.resources.getString(R.string.phone_number, item.phoneNumber)
            itemView.txtViewEmail.text =
                itemView.resources.getString(R.string.email, item.schoolEmail)
        }

    }

    override fun getFilter(): Filter {
        return object : Filter() {
            override fun performFiltering(p0: CharSequence?): FilterResults {
                val searchChar = p0.toString()
                val result = ArrayList<School>()
                if (searchChar.isNotEmpty()) {
                    for (i in oldSchoolArray) {
                        if (i.schoolName.toLowerCase(Locale.ROOT)
                                .contains(searchChar.toLowerCase(Locale.ROOT))
                        ) {
                            result.add(i)
                        }
                    }
                }
                val filteredResult = FilterResults()

                filteredResult.values = result
                return filteredResult
            }

            override fun publishResults(p0: CharSequence?, p1: FilterResults?) {
                if (arrayOf(p1!!.values).size > 0 && p0!!.length > 0) {
                    updateSchoolListItems(p1.values as ArrayList<School>)
                } else {
                    data = oldSchoolArray
                }

            }

        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val view =
            LayoutInflater.from(parent.context).inflate(R.layout.adapter_school_item, parent, false)
        return MyViewHolder(view)
    }

    override fun getItemCount() = data.size

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.bindItem(data[position])
    }

    fun updateSchoolListItems(schools: ArrayList<School>) {
        val diffCallback = SchoolDiffCallback(oldSchoolArray, schools)
        val diffResult = DiffUtil.calculateDiff(diffCallback)
        data = schools
        diffResult.dispatchUpdatesTo(this)
    }
}