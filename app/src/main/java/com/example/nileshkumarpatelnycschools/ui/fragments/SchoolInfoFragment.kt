package com.example.nileshkumarpatelnycschools.ui.fragments

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.fragment.app.commit
import androidx.fragment.app.replace
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.example.nileshkumarpatelnycschools.R
import com.example.nileshkumarpatelnycschools.data.Result
import com.example.nileshkumarpatelnycschools.data.models.School
import com.example.nileshkumarpatelnycschools.data.models.Score
import com.example.nileshkumarpatelnycschools.utilities.Constants
import com.example.nileshkumarpatelnycschools.viewmodels.ScoreViewModel
import com.example.nileshkumarpatelnycschools.viewmodels.ViewModelFactoryProvider
import dagger.android.support.AndroidSupportInjection
import kotlinx.android.synthetic.main.fragment_school_info.*
import kotlinx.android.synthetic.main.fragment_school_info.progressBar
import kotlinx.android.synthetic.main.fragment_school_info.txtViewError
import javax.inject.Inject


class SchoolInfoFragment : Fragment() {

    @Inject
    lateinit var viewModelFactoryProvider: ViewModelFactoryProvider
    private var score: List<Score> = ArrayList()
    private lateinit var scoreViewModel: ScoreViewModel

    private lateinit var schoolAdditionalInfo: School

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_school_info, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initialization()
    }

    override fun onAttach(context: Context) {
        AndroidSupportInjection.inject(this)
        super.onAttach(context)
    }

    /**
     * initialization for view customization
     */
    private fun initialization() {
        //Get Parcel with additional details of school with all required details on click item
        schoolAdditionalInfo = requireArguments().getParcelable(Constants.BUNDLE_DATA)!!

        //Set result and observe from viewModel and get Score
        scoreViewModel =
            ViewModelProvider(this, viewModelFactoryProvider)[ScoreViewModel::class.java]

        observedData()
    }

    /**
     * while fetching data, Observing data from viewModel
     */

    private fun observedData() {

        txtViewOverview.text = schoolAdditionalInfo.overViewParagraph
        schoolName.text = schoolAdditionalInfo.schoolName

        scoreViewModel.fetchScore(schoolAdditionalInfo.dbn)
            .observe(viewLifecycleOwner, Observer {
                when (it) {
                    is Result.SUCCESS -> {
                        score = it.data
                        if (score.isNotEmpty()) {
                            groupItems.visibility = View.VISIBLE
                            txtSATAvgWriting.text = resources.getString(
                                R.string.sat_avg_writing_score,
                                score[0].satWritingAvgScore
                            )
                            txtSATAvgMath.text = resources.getString(
                                R.string.sat_math_avg_score,
                                score[0].satMathAvgScore
                            )
                            txtSATAvgReading.text = resources.getString(
                                R.string.sat_avg_reading_score,
                                score[0].satCriticalReadingAvgScore
                            )
                        } else {
                            groupItems.visibility = View.GONE
                        }
                        progressBar.visibility = View.GONE
                        txtViewError.visibility = View.GONE
                    }

                    is Result.ERROR -> {
                        exceptionGroup.visibility = View.GONE
                        txtViewError.visibility = View.VISIBLE
                    }

                    is Result.LOADING -> {
                        progressBar.visibility = View.VISIBLE
                        txtViewError.visibility = View.GONE
                    }
                }
            })

        schoolURLHandling()
    }

    /**
     * Click on website of school Open Webview for website visit
     */

    private fun schoolURLHandling() {
        txtViewWebSite.text = schoolAdditionalInfo.website

        txtViewWebSite.setOnClickListener {

            requireActivity().supportFragmentManager.commit {
                setReorderingAllowed(true)
                replace<WebViewFragment>(
                    R.id.fragmentView,
                    Constants.WEB_VIEW_FRAGMENT,
                    bundleOf("url" to (schoolAdditionalInfo.website))
                )
                addToBackStack(this.toString())
            }
        }
    }

}