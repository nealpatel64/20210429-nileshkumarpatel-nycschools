package com.example.nileshkumarpatelnycschools.ui.fragments

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.View
import android.webkit.WebView
import android.webkit.WebViewClient
import androidx.fragment.app.Fragment
import com.example.nileshkumarpatelnycschools.R
import kotlinx.android.synthetic.main.fragment_webview.*

class WebViewFragment : Fragment(R.layout.fragment_webview) {

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        initialization()
        super.onViewCreated(view, savedInstanceState)
    }


    @SuppressLint("SetJavaScriptEnabled")
    private fun initialization() {
        val loadURL = requireArguments().getString("url")
        displayWebView.apply {
            settings.javaScriptEnabled = true
            webViewClient = object : WebViewClient() {
                override fun onPageFinished(view: WebView?, url: String?) {
                    if (progressBar != null) {
                        progressBar.visibility = View.GONE
                    }
                    super.onPageFinished(view, url)
                }
            }
            loadUrl(loadURL!!)
        }
    }
}