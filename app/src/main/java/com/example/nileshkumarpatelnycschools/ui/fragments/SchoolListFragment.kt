package com.example.nileshkumarpatelnycschools.ui.fragments

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.SearchView
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.fragment.app.commit
import androidx.fragment.app.replace
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.nileshkumarpatelnycschools.R
import com.example.nileshkumarpatelnycschools.data.Result
import com.example.nileshkumarpatelnycschools.data.models.School
import com.example.nileshkumarpatelnycschools.ui.adapters.SchoolsAdapter
import com.example.nileshkumarpatelnycschools.utilities.Constants
import com.example.nileshkumarpatelnycschools.viewmodels.SchoolViewModel
import com.example.nileshkumarpatelnycschools.viewmodels.ViewModelFactoryProvider
import dagger.android.support.AndroidSupportInjection
import kotlinx.android.synthetic.main.fragment_school.*
import kotlinx.android.synthetic.main.fragment_school.view.*
import javax.inject.Inject

/**
 * In future, can do some animation work with RecycleView to display data with CardView,
 * As mention in repositoryClass if API supporting paging with paging data  in response with pageInfo,
 * Can add Header and footer handling with Paging Adapter (Error handling for infinity scrolling)
 */

class SchoolListFragment : Fragment() {

    @Inject
    lateinit var schoolAdapter: SchoolsAdapter

    @Inject
    lateinit var viewModelFactoryProvider: ViewModelFactoryProvider
    private lateinit var schoolViewModel: SchoolViewModel

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_school, container, false)
    }

    override fun onAttach(context: Context) {
        AndroidSupportInjection.inject(this)
        super.onAttach(context)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initialization(view)
    }

    private fun initialization(view: View) {
        schoolViewModel = ViewModelProvider(
            requireActivity(),
            viewModelFactoryProvider
        )[SchoolViewModel::class.java]

        view.rv.apply {
            adapter = schoolAdapter
            layoutManager = LinearLayoutManager(view.context)
            setHasFixedSize(true)
        }

        btnTryAgain.setOnClickListener {
            observedData()
        }

        observedData()
    }

    /**
     * If user come from the back screen disabled search view and keyboard
     */
    override fun onStart() {
        super.onStart()
        searchSchool.clearFocus()
        searchSchool.isIconified = true
    }

    /**
     * Observed data from school list viewModel and display to the UI
     */

    private fun observedData() {

        schoolViewModel.fetchSchool().observe(viewLifecycleOwner, Observer {

            when (it) {
                is Result.SUCCESS -> {
                    rv.visibility = View.VISIBLE
                    schoolAdapter.data = it.data as ArrayList<School>
                    progressBar.visibility = View.GONE
                    txtViewError.visibility = View.GONE
                    btnTryAgain.visibility = View.GONE
                }
                is Result.ERROR -> {
                    txtViewError.visibility = View.VISIBLE
                    rv.visibility = View.GONE
                    progressBar.visibility = View.GONE
                    btnTryAgain.visibility = View.VISIBLE
                }

                is Result.LOADING -> {
                    progressBar.visibility = View.VISIBLE
                    rv.visibility = View.GONE
                    txtViewError.visibility = View.GONE
                    btnTryAgain.visibility = View.GONE
                }
            }
        })

        handleAdapterClicked()
        searchQuery()
    }

    /**
     *  When user type in searchView check data is available or not, Whatever user type in search
     *  view pass argument of String/character to filtered through listener
     */

    private fun searchQuery() {
        searchSchool.clearFocus()
        searchSchool.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(p0: String?): Boolean {
                searchSchool.clearFocus()
                return false
            }

            override fun onQueryTextChange(p0: String?): Boolean {
                schoolAdapter.filter.filter(p0)
                return false
            }
        })
    }

    /**
     * When user click on school for details navigate to other screen
     */

    private fun handleAdapterClicked() {
        schoolAdapter.onItemClicked = { it ->
            requireActivity().supportFragmentManager.commit {
                replace<SchoolInfoFragment>(
                    R.id.fragmentView,
                    Constants.ROOT_FRAGMENT,
                    bundleOf(Constants.BUNDLE_DATA to it)
                )
                addToBackStack(this@SchoolListFragment.toString())
            }
        }
    }

}