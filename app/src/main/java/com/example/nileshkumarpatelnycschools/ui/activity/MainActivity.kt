package com.example.nileshkumarpatelnycschools.ui.activity

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.commit
import androidx.fragment.app.add
import com.example.nileshkumarpatelnycschools.R
import com.example.nileshkumarpatelnycschools.ui.fragments.SchoolListFragment
import dagger.android.AndroidInjection

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        supportFragmentManager.commit {
            setReorderingAllowed(true)
            add<SchoolListFragment>(R.id.fragmentView)
        }
    }
}
