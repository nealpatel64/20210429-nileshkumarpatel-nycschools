package com.example.nileshkumarpatelnycschools.ui.adapters

import androidx.annotation.Nullable
import androidx.recyclerview.widget.DiffUtil
import com.example.nileshkumarpatelnycschools.data.models.School

/**
 * Use abstract class, DifUtil.CallBack()
 * used as callback class by DiffUtil to calculate the difference between two lists
 * Improve the performance while use search in school list
 */

class SchoolDiffCallback(
    private val oldList: ArrayList<School>,
    private val newList: ArrayList<School>
) :
    DiffUtil.Callback() {

    override fun getOldListSize() = oldList.size

    override fun getNewListSize() = newList.size

    override fun areItemsTheSame(
        oldItemPosition: Int,
        newItemPosition: Int
    ) = oldList[oldItemPosition] == newList[newItemPosition]

    override fun areContentsTheSame(
        oldItemPosition: Int,
        newItemPosition: Int
    ) = true

    @Nullable
    override fun getChangePayload(
        oldItemPosition: Int,
        newItemPosition: Int
    ): Any? {
        return super.getChangePayload(oldItemPosition, newItemPosition)
    }
}