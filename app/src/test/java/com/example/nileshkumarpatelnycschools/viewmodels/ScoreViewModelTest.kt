package com.example.nileshkumarpatelnycschools.viewmodels

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.Observer
import com.example.nileshkumarpatelnycschools.TestCoroutineRule
import com.example.nileshkumarpatelnycschools.data.Result
import com.example.nileshkumarpatelnycschools.data.models.Score
import com.example.nileshkumarpatelnycschools.data.repositories.ScoreRepository
import com.example.nileshkumarpatelnycschools.utilities.Constants
import kotlinx.coroutines.ExperimentalCoroutinesApi
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.Mockito.doReturn
import org.mockito.Mockito.verify
import org.mockito.junit.MockitoJUnitRunner
import java.lang.RuntimeException

/**
 * Used Mockito, Junit inorder to test perform Unit testing in ViewModel
 * Cover test case of fetch Score of selected school and {@exception} scenario when server throw any error
 * Used Coroutine Testing APIs with mock repository for server request and response Unit test
 * Used {@InstantTaskExecutorRule} to test code with LiveData
 */

@ExperimentalCoroutinesApi
@RunWith(MockitoJUnitRunner::class)
class ScoreViewModelTest {


    @get:Rule
    val testCoroutineRule = TestCoroutineRule()

    @get:Rule
    val instantTaskExecutorRule = InstantTaskExecutorRule()

    @Mock
    lateinit var repository: ScoreRepository

    @Mock
    lateinit var scoreObserver: Observer<Result<List<Score>>>

    private lateinit var viewModel: ScoreViewModel

    /**
     * Unit test case to test ViewModel with mock repository to get the score of selected school with 200 status code response
     */

    @Test
    fun should_Success_When_200ResponseOnSchoolFetch() {

        testCoroutineRule.runBlockingTest {

            doReturn(emptyList<Score>())
                .`when`(repository)
                .getScoresData(Constants.TESTING_SCORE_QUERY)

            viewModel = ScoreViewModel(repository)

            viewModel.fetchScore(Constants.TESTING_SCORE_QUERY).observeForever(scoreObserver)

            verify(repository).getScoresData(Constants.TESTING_SCORE_QUERY)

            verify(scoreObserver).onChanged(Result.SUCCESS(emptyList()))

            viewModel.fetchScore(Constants.TESTING_SCORE_QUERY).removeObserver(scoreObserver)
        }

    }

    /**
     * Unit test case to test ViewModel with mock repository to get the score of selected school failure scenario handling
     * when server is down, maybe no internet case or any {@exception} use case
     */

    @Test
    fun should_ThrowException_When_SchoolApiHaveAnyError() {
        testCoroutineRule.runBlockingTest {
            Mockito.doThrow(RuntimeException(Constants.TESTING_EXPECTED_ERROR))
                .`when`(repository)
                .getScoresData(Constants.TESTING_SCORE_QUERY)

            viewModel = ScoreViewModel(repository)

            viewModel.fetchScore(Constants.TESTING_SCORE_QUERY).observeForever(scoreObserver)

            verify(repository).getScoresData(Constants.TESTING_SCORE_QUERY)

            verify(scoreObserver).onChanged(Result.ERROR(Constants.TESTING_EXPECTED_ERROR))

            viewModel.fetchScore(Constants.TESTING_SCORE_QUERY).removeObserver(scoreObserver)
        }
    }

}