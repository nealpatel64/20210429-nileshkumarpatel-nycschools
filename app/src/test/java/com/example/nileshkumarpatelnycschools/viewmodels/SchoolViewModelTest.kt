package com.example.nileshkumarpatelnycschools.viewmodels

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.Observer
import com.example.nileshkumarpatelnycschools.TestCoroutineRule
import com.example.nileshkumarpatelnycschools.data.Result
import com.example.nileshkumarpatelnycschools.data.models.School
import com.example.nileshkumarpatelnycschools.data.repositories.SchoolsRepository
import com.example.nileshkumarpatelnycschools.utilities.Constants
import kotlinx.coroutines.ExperimentalCoroutinesApi
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito.*
import org.mockito.junit.MockitoJUnitRunner
import java.lang.RuntimeException

/**
 * Used Mockito, Junit inorder to test perform Unit testing in ViewModel
 * Cover test case of fetch School list, and {@exception} scenario when server throw any error
 * Used Coroutine Testing APIs with mock repository for server request and response Unit testing
 * Used {@InstantTaskExecutorRule} to test code with LiveData
 */

@ExperimentalCoroutinesApi
@RunWith(MockitoJUnitRunner::class)
class SchoolViewModelTest {

    @get:Rule
    val testInstantTaskExecutorRule = InstantTaskExecutorRule()

    @get:Rule
    val testCoroutineRule = TestCoroutineRule()

    @Mock
    private lateinit var repository: SchoolsRepository

    @Mock
    private lateinit var schoolObserver: Observer<Result<List<School>>>

    private lateinit var viewModel: SchoolViewModel

    /**
     * Unit test case to test viewmodel with mock repository to get the school list
     */

    @Test
    fun should_Success_When_200ResponseOnScoreFetch() {
        testCoroutineRule.runBlockingTest {
            doReturn(emptyList<School>())
                .`when`(repository).getSchools()
            viewModel = SchoolViewModel(repository)
            viewModel.fetchSchool().observeForever(schoolObserver)
            verify(repository).getSchools()
            verify(schoolObserver).onChanged(Result.SUCCESS(emptyList()))
            viewModel.fetchSchool().removeObserver(schoolObserver)
        }
    }

    /**
     * Unit test case to test viewmodel with mock repository to get the school list on failure scenario handling,
     * when server is down, maybe no internet case or any {@exception} use case
     */

    @Test
    fun should_ThrowException_When_ScoreApiHaveAnyError() {
        testCoroutineRule.runBlockingTest {
            doThrow(RuntimeException(Constants.TESTING_EXPECTED_ERROR))
                .`when`(repository)
                .getSchools()
            viewModel = SchoolViewModel(repository)
            viewModel.fetchSchool().observeForever(schoolObserver)
            verify(repository).getSchools()
            verify(schoolObserver).onChanged(Result.ERROR(Constants.TESTING_EXPECTED_ERROR))
            viewModel.fetchSchool().removeObserver(schoolObserver)
        }
    }

}